FROM fedora:latest
MAINTAINER rytone "rytonemail@gmail.com"

RUN dnf upgrade -y
RUN dnf install git gcc gcc-c++ make cmake ninja-build libcurl-devel openssl-devel rapidjson  websocketpp-devel -y

COPY dockerscripts/install_cpr.sh /
COPY dockerscripts/install_spdlog.sh /
COPY dockerscripts/install_fmt.sh /

RUN sh install_cpr.sh
RUN sh install_spdlog.sh
RUN sh install_fmt.sh

RUN rm install_cpr.sh install_spdlog.sh install_fmt.sh
