git clone https://github.com/whoshuu/cpr
cd cpr
cmake . -DBUILD_CPR_TESTS=OFF -DUSE_SYSTEM_CURL=ON
make
cp -R include /usr
cp -R lib/* /usr/lib64
cd ..
rm -rf cpr
