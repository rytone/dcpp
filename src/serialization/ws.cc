#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

#include <fmt/format.h>

#include "dcpp/serialization/ws.hpp"
#include "dcpp/const.hpp"

std::string dcpp::serialize::ws::identify(std::string token, int shard, int shardCount) {
	rapidjson::StringBuffer buf;
	rapidjson::Writer<rapidjson::StringBuffer> writer(buf);

	writer.StartObject();
	
	writer.Key("op");
	writer.Int(2);
	
	writer.Key("d");
	writer.StartObject();

	writer.Key("token");
	writer.String(token.c_str());

	writer.Key("properties");
	writer.StartObject();

	writer.Key("$os");
	writer.String("linux");

	writer.Key("$browser");
	writer.String(fmt::format("DCPP {}", dcpp::constant::VERSION).c_str());

	writer.Key("$device");
	writer.String(fmt::format("DCPP {}", dcpp::constant::VERSION).c_str());

	writer.Key("$referrer");
	writer.String("");
	
	writer.Key("$referring_domain");
	writer.String("");

	writer.EndObject();

	writer.Key("compress");
	writer.Bool(false);

	writer.Key("large_threshold");
	writer.Int(250);

	writer.Key("shard");
	writer.StartArray();

	writer.Int(shard);
	writer.Int(shardCount);

	writer.EndArray();

	writer.EndObject();

	writer.EndObject();

	return buf.GetString();
}

std::string dcpp::serialize::ws::heartbeat(int seq) {
	rapidjson::StringBuffer buf;
	rapidjson::Writer<rapidjson::StringBuffer> writer(buf);

	writer.StartObject();

	writer.Key("op");
	writer.Int(1);

	writer.Key("d");
	writer.Int(seq);

	writer.EndObject();
	
	return buf.GetString();
}
