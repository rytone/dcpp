#include "dcpp/dcpp.hpp"

#include <spdlog/spdlog.h>

void dcpp::init(spdlog::level::level_enum log_level) {
	auto dcppLog = spdlog::stdout_color_mt("dcpp");
	auto restLog = spdlog::stdout_color_mt("dcpp_rest");
	auto wsLog   = spdlog::stdout_color_mt("dcpp_ws");
	
	dcppLog->set_level(log_level);	
	restLog->set_level(log_level);
	wsLog->set_level(log_level);
}

void dcpp::done() {
	spdlog::drop("dcpp");
	spdlog::drop("dcpp_rest");
	spdlog::drop("dcpp_ws");
}
