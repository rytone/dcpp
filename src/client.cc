#include "dcpp/client.hpp"
#include "dcpp/const.hpp"
#include "dcpp/deserialization/rest.hpp"
#include "dcpp/deserialization/common.hpp"
#include "dcpp/ws/client.hpp"
#include "dcpp/objects.hpp"

#include <fmt/format.h>

#include <cpr/cpr.h>

#include <rapidjson/stringbuffer.h>
#include <rapidjson/writer.h>

dcpp::client::client(dcpp::token type, std::string token) {
	log      = spdlog::get("dcpp");
	rest_log = spdlog::get("dcpp_rest");

	log->debug("creating new client");

	switch (type) {
	case dcpp::bot:
		log->debug("token is bot");
		auth = fmt::format("Bot {}", token);
		break;
	case dcpp::bearer:
		log->debug("token is bearer");
		auth = fmt::format("Bearer {}", token);
		break;
	}
}

dcpp::client::~client() {
	log->debug("client destructor called, cleaning up");
	delete ws_client;
}

void dcpp::client::open_ws() {
	ws_client = new dcpp::ws::client(this);
	ws_client->open(this->gateway(), auth);
}

std::string dcpp::client::gateway() {
	rest_log->debug("sending GET to /gateway");
	auto r = cpr::Get(cpr::Url{"https://discordapp.com/api/gateway"});
	rest_log->debug("got response: {}", r.text);
	if (r.status_code != 200) {
		rest_log->warn("response was http {}", r.status_code);
		return "";
	}
	return dcpp::deserialize::rest::gateway(r.text);
}

dcpp::object::message dcpp::client::send_message(std::string channel_id, std::string content, bool tts) {
	rest_log->debug("serializing message");

	rapidjson::StringBuffer buf;
	rapidjson::Writer<rapidjson::StringBuffer> writer(buf);

	writer.StartObject();

	writer.Key("content");
	writer.String(content.c_str());

	writer.Key("tts");
	writer.Bool(tts);

	writer.EndObject();

	rest_log->debug("serialized to {}", buf.GetString());
	
	rest_log->debug("sending POST to /channels/{}/messages", channel_id);
	auto r = cpr::Post(
			cpr::Url{fmt::format("https://discordapp.com/api/channels/{}/messages", channel_id)},
			cpr::Body{buf.GetString()},
			cpr::Header{{"Authorization", auth}});
	rest_log->debug("got response: {}", r.text);
	if (r.status_code != 200) {
		rest_log->warn("response was http {}", r.status_code);
		return dcpp::object::message{};
	}
	return dcpp::deserialize::common::message(r.text);
}
