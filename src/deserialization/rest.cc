#include "dcpp/deserialization/rest.hpp"

#include <rapidjson/document.h>

std::string dcpp::deserialize::rest::gateway(std::string raw) {
	rapidjson::Document json;
	json.Parse(raw.c_str());
	return json["url"].GetString();
}
