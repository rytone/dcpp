#include <rapidjson/document.h>

#include "dcpp/deserialization/ws.hpp"
#include "dcpp/objects.hpp"

dcpp::object::presence_update dcpp::deserialize::ws::presence_update(std::string raw) {
	rapidjson::Document json;
	json.Parse(raw.c_str());
	return dcpp::deserialize::ws::presence_update(json);
}

dcpp::object::presence_update dcpp::deserialize::ws::presence_update(rapidjson::Value& json) {
	dcpp::object::presence_update pu;
	
	pu.user_id = json["user"]["id"].GetString();
	for (rapidjson::SizeType i = 0; i < json["roles"].Size(); i++) {
		pu.roles.push_back(json["roles"][i].GetString());
	}
	if (!json["game"].IsNull()) pu.game = json["game"]["name"].GetString();
	pu.guild_id = json["guild_id"].GetString();
	pu.status = json["status"].GetString();

	return pu;
}
