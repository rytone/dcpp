#include <rapidjson/document.h>

#include "dcpp/deserialization/common.hpp"
#include "dcpp/objects.hpp"

dcpp::object::message dcpp::deserialize::common::message(std::string raw) {
	rapidjson::Document json;
	json.Parse(raw.c_str());
	return dcpp::deserialize::common::message(json);
}

dcpp::object::message dcpp::deserialize::common::message(rapidjson::Value& json) {
	dcpp::object::message msg;

	msg.id = json["id"].GetString();
	msg.channel_id = json["channel_id"].GetString();
	// TODO author
	msg.content = json["content"].GetString();
	msg.timestamp = json["timestamp"].GetString();
	if (!json["edited_timestamp"].IsNull()) msg.edited_timestamp = json["edited_timestamp"].GetString();
	msg.tts = json["tts"].GetBool();
	msg.mention_everyone = json["mention_everyone"].GetBool();
	// TODO mentions
	// TODO mention_roles
	// TODO attatchments
	// fuck embeds
	// fuck reactions
	msg.pinned = json["pinned"].GetBool();
	if (json.HasMember("webhook_id")) msg.webhook_id = json["webhook_id"].GetString();

	return msg;
}
