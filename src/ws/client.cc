#include <thread>
#include <chrono>

#include <websocketpp/config/asio_client.hpp>
#include <websocketpp/client.hpp>

#include <spdlog/spdlog.h>

#include <rapidjson/document.h>

#include "dcpp/ws/client.hpp"
#include "dcpp/client.hpp"
#include "dcpp/serialization/ws.hpp"
#include "dcpp/deserialization/common.hpp"
#include "dcpp/deserialization/ws.hpp"

typedef websocketpp::client<websocketpp::config::asio_tls_client> wspp_client;
typedef websocketpp::config::asio_tls_client::message_type::ptr msg_ptr;
typedef websocketpp::lib::shared_ptr<websocketpp::lib::asio::ssl::context> ctx_ptr;

using websocketpp::lib::placeholders::_1;
using websocketpp::lib::placeholders::_2;
using websocketpp::lib::bind;

ctx_ptr on_tls_init(websocketpp::connection_hdl) {
	ctx_ptr ctx = websocketpp::lib::make_shared<boost::asio::ssl::context>(boost::asio::ssl::context::sslv23);
	ctx->set_options(boost::asio::ssl::context::default_workarounds |
					boost::asio::ssl::context::no_sslv2 |
					boost::asio::ssl::context::no_sslv3 |
					boost::asio::ssl::context::single_dh_use);
	ctx->set_verify_mode(0);
	return ctx;
}

void on_open(wspp_client* c, dcpp::ws::client* d, websocketpp::connection_hdl hdl) {
	d->ws_log->debug("websocket connection opened");
}

void heartbeat(wspp_client* c, dcpp::ws::client* d, websocketpp::connection_hdl hdl, int dur) {
	for (;;) {
		websocketpp::lib::error_code e;
		d->ws_log->debug("sending heartbeat, last seq = {}", d->last_seq);
		c->send(hdl, dcpp::serialize::ws::heartbeat(d->last_seq), websocketpp::frame::opcode::text, e);
		std::this_thread::sleep_for(std::chrono::milliseconds(dur));
	}
}

void on_message(wspp_client* c, dcpp::ws::client* d, websocketpp::connection_hdl hdl, msg_ptr msg) {
	d->ws_log->debug("got ws message on hdl {} with message: {}", hdl.lock().get(), msg->get_payload());
	rapidjson::Document json;
	json.Parse(msg->get_payload().c_str());
	int opcode = json["op"].GetInt();
	
	if (!json["s"].IsNull()) d->last_seq = json["s"].GetInt();

	switch (opcode) {
		case 0: //dispatch
			{
				std::string evt = json["t"].GetString();
				d->ws_log->debug("got dispatch event {}", evt);

				if (evt == "READY") {
					if (d->dcpp_client->events.ready) {
						d->ws_log->debug("firing ready event");
						d->dcpp_client->events.ready(d->dcpp_client);
					}
				} else if (evt == "MESSAGE_CREATE") {
					if (d->dcpp_client->events.message_create)  {
						dcpp::object::message msg = dcpp::deserialize::common::message(json["d"]);
						d->ws_log->debug("firing message create event");
						d->dcpp_client->events.message_create(d->dcpp_client, msg);
					}
				} else if (evt == "PRESENCE_UPDATE") {
					if (d->dcpp_client->events.presence_update) { 
						dcpp::object::presence_update pu = dcpp::deserialize::ws::presence_update(json["d"]);
						d->ws_log->debug("firing presence update event");
						d->dcpp_client->events.presence_update(d->dcpp_client, pu);
					}
				}

				break;
			}

		case 10: // hello
			{
				std::string identify = dcpp::serialize::ws::identify(d->auth, 0, 1);
				d->ws_log->debug("got op 10 (hello), sending identify: {}", identify);
				websocketpp::lib::error_code e;
				c->send(hdl, identify, websocketpp::frame::opcode::text, e);
				if (e) {
					d->ws_log->error("error sending identify: {}", e.message());
				}
				d->ws_log->debug("starting heartbeat...");
				d->heartbeat_thread = std::thread(heartbeat, c, d, hdl, json["d"]["heartbeat_interval"].GetInt());
				break;
			}
	}
}

dcpp::ws::client::client(dcpp::client* c) {
	last_seq = 0;

	log = spdlog::get("dcpp");
	ws_log = spdlog::get("dcpp_ws");

	log->debug("creating new websocket client");

	dcpp_client = c;

	ws_client.clear_access_channels(websocketpp::log::alevel::all);
	ws_client.set_access_channels(websocketpp::log::alevel::connect);

	ws_log->debug("setting websocketpp evt handlers");
	ws_client.set_tls_init_handler(&on_tls_init);
	ws_client.set_open_handler(bind(&on_open, &ws_client, this, ::_1));
	ws_client.set_message_handler(bind(&on_message, &ws_client, this, ::_1, ::_2));	

	ws_log->debug("initializing asio");
	ws_client.init_asio();
}

void dcpp::ws::client::open(std::string gateway, std::string token) {
	auth = token;

	ws_log->debug("creating connection");
	websocketpp::lib::error_code e;
	wspp_client::connection_ptr con = ws_client.get_connection(gateway + "/?v=5&encoding=json", e);

	if (e) {
		ws_log->error("error opening ws connection: {}", e.message());
		return;
	}

	ws_log->debug("executing ws_client.connect()");
	ws_client.connect(con);
	ws_log->debug("executing ws_client.run()");
	ws_client.run();
}
