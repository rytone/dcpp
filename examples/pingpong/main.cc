#include <dcpp/dcpp.hpp>
#include <dcpp/client.hpp>
#include <dcpp/objects.hpp>

#include <spdlog/spdlog.h>

std::shared_ptr<spdlog::logger> bot_log;

void ready_handle(dcpp::client* c) {
	bot_log->info("ready");
}

void message_handle(dcpp::client* c, dcpp::object::message m) {
	bot_log->info("got message: {}", m.content);
	if (m.content == "//ping") {
		c->send_message(m.channel_id, "pong with BLAZING C++ SPEED", false);
	}
}

void presence_update_handle(dcpp::client* c, dcpp::object::presence_update p) {
};

int main(int argc, char *argv[]) {
	if (argc < 2) return -1;
	bot_log = spdlog::stdout_color_mt("bot");

	dcpp::init(spdlog::level::debug);

	dcpp::client* c = new dcpp::client(dcpp::bot, argv[1]);

	c->events.ready = ready_handle;
	c->events.message_create = message_handle;
	c->events.presence_update = presence_update_handle;
	c->open_ws();

	delete c;

	return 0;
}
