# dcpp [![build status](https://gitlab.com/rytone/dcpp/badges/master/build.svg)](https://gitlab.com/rytone/dcpp/commits/master)
c++ library for the discord api

### dependencies
 - ninja
 - cpr
 - rapidjson
 - spdlog
 - fmt
 - websocketpp
 - cldoc (documentation)

### building
`ninja`, `ninja docs`, `ninja ex_[example name]`

### v1 when?
rest api fully covered + connect to websocket thingymajig

### usage
check examples/pingpong for now

### todo
 - [ ] cover all rest endpoints
 - [x] connect to websocket
 - [ ] track state
 - [ ] voice
 - [ ] whatever else discord implements in the far far future

### contributing
  - fork
  - make changes
  - make sure its formatted right (1TBS, 4 SPACE WIDTH HARD TABS, PLEASE!)
  - pr
  - yes
