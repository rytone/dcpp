#pragma once

#include <string>
#include <vector>

namespace dcpp {
	namespace object {
		class user {
			public:
				std::string id;
				std::string username;
				std::string discriminator;
				std::string avatar;
				bool bot;
				bool mfa_enabled;
				bool verified;
				std::string email;
		};
		class role {
			public:
				std::string id;
				std::string name;
				int color;
				bool hoist;
				int position;
				int permissions;
				bool managed;
				bool mentionable;
		};
		class attatchment {
			public:
				std::string id;
				std::string filename;
				int size;
				std::string url;
				std::string proxy_url;
				int width;
				int height;
		};
		class message {
			public:
				std::string id;
				std::string channel_id;
				user author;
				std::string content;
				std::string timestamp;
				std::string edited_timestamp;
				bool tts;
				bool mention_everyone;
				std::vector<user> mentions;
				std::vector<role> mention_roles;
				std::vector<attatchment> attatchments;
				// TODO embeds
				// TODO reactions
				std::string nonce;
				bool pinned;
				std::string webhook_id;
		};
		class presence_update {
			public:
				std::string user_id;
				std::vector<std::string> roles;
				std::string game;
				std::string guild_id;
				std::string status;
		};
	};
};
