#pragma once

#include <string>

namespace dcpp {
	namespace serialize {
		namespace ws {
			std::string identify(std::string, int, int);
			std::string heartbeat(int);
		};
	};
};
