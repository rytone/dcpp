#pragma once

#include "dcpp/objects.hpp"
#include "dcpp/ws/client.hpp"
#include "dcpp/ws/events.hpp"

#include <string>

#include <spdlog/spdlog.h>

namespace dcpp {
	enum token {bot, bearer};

	class client {
		public:
			dcpp::ws::events events;

			void open_ws();

			std::string gateway();
			dcpp::object::message send_message(std::string, std::string, bool);

			client(token, std::string);
			~client();
		private:
			dcpp::ws::client* ws_client;
			std::string auth;
			std::shared_ptr<spdlog::logger> log;
			std::shared_ptr<spdlog::logger> rest_log;
	};
};
