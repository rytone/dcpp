#pragma once

#include <functional>

#include "dcpp/objects.hpp"

namespace dcpp {
	class client;
	namespace ws {
		struct events {
			std::function<void(dcpp::client*)> ready;	
			std::function<void(dcpp::client*, dcpp::object::message)> message_create;
			std::function<void(dcpp::client*, dcpp::object::presence_update)> presence_update;
		};
	};
}
