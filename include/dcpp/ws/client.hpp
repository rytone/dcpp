#pragma once

#include <string>
#include <thread>

#include <spdlog/spdlog.h>

#include <websocketpp/config/asio_client.hpp>
#include <websocketpp/client.hpp>

#include "dcpp/ws/events.hpp"

namespace dcpp {
	namespace ws {
		class client {
			public:
				dcpp::client* dcpp_client;
				websocketpp::client<websocketpp::config::asio_tls_client> ws_client;
				std::shared_ptr<spdlog::logger> log;
				std::shared_ptr<spdlog::logger> ws_log;
				std::string auth;
				std::thread heartbeat_thread;
				int last_seq;

				client(dcpp::client*);
				void open(std::string, std::string);
		};
	};
};
