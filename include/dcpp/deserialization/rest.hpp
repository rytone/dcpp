#pragma once

#include <string>

namespace dcpp {
	namespace deserialize {
		namespace rest {
			std::string gateway(std::string);
		};
	};
};
