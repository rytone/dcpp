#pragma once

#include <string>

#include <rapidjson/document.h>

#include "dcpp/objects.hpp"

namespace dcpp {
	namespace deserialize {
		namespace common {
			dcpp::object::message message(std::string);
			dcpp::object::message message(rapidjson::Value&);
		};
	};
};
