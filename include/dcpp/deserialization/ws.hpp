#pragma once

#include <string>

#include <rapidjson/document.h>

#include "dcpp/objects.hpp"

namespace dcpp {
	namespace deserialize {
		namespace ws {
			dcpp::object::presence_update presence_update(std::string);
			dcpp::object::presence_update presence_update(rapidjson::Value&);
		};
	};
};
