#pragma once

#include <spdlog/spdlog.h>

namespace dcpp {
	/* Initializes DCPP
	 *
	 * This creates all of the spdlog loggers and initializes cURL with restclient-cpp.
	 */
	void init(spdlog::level::level_enum log_level = spdlog::level::info);

	/* Cleans up DCPP.
	 *
	 * This closes up all of the loggers and cleans up cURL with restclient-cpp.
	 */
	void done();
};
